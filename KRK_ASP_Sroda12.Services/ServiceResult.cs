﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRK_ASP_Sroda12.Services
{
    public enum ServiceResultStatus
    {
        [Description("Błąd")]
        Error = 0,

        [Description("Sukces")]
        Succes = 1,

        [Description("Ostrzeżenie")]
        Warrnig,


        [Description("Informacja")]
        Info,
    }
    public class ServiceResult
    {
        public ServiceResultStatus Result { get; set; }

        public ICollection<String> Messages { get; set; }

        public ServiceResult()
        {
            Result = ServiceResultStatus.Succes;
            Messages = new List<string>();
        }

    }
}
