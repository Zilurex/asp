﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRK_ASP_Sroda12.Services
{
    public class ServicesAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<ContractExportService>().As<IContractExportService>().InstancePerRequest().PropertiesAutowired();
            // builder.RegisterType<EmailService>().As<IEmailService>().InstancePerLifetimeScope().PropertiesAutowired();

            builder.RegisterGeneric(typeof(RepositoryService<>)).As(typeof(IRepositoryService<>))
                .InstancePerRequest() ;
        }
    }
}
