﻿using KRK_ASP_Sroda12.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KRK_ASP_Sroda12.Services
{
    public interface IRepositoryService<T> where T : IEntity<int>
    {
        IQueryable<T> GetAll();
        T GetSingle(int id);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        ServiceResult Add(T entity);
        ServiceResult Delete(T entity);
        ServiceResult Edit(T entity);
        ServiceResult Save();

    }

    public class RepositoryService<T> : IRepositoryService<T> where T : class, IEntity<int>
    {
        protected IDbContext _context;
        protected DbSet<T> _set;

        public RepositoryService(IDbContext context)
        {
            _context = context;
            _set = (_context as DbContext).Set<T>();
        }

        public virtual ServiceResult Add(T entity)
        {
            ServiceResult result = new ServiceResult();
            try
            { 
                _set.Add(entity);
                result = Save();
            }
            catch (Exception e)
            {
                result.Result = ServiceResultStatus.Error;
                result.Messages.Add(e.Message);
            }

            return result;
        }

        public virtual ServiceResult Delete(T entity)
        {
            ServiceResult result = new ServiceResult();
            try
            {
                _set.Remove(entity);
                result = Save();
            }
            catch (Exception e)
            {
                result.Result = ServiceResultStatus.Error;
                result.Messages.Add(e.Message);
            }
            return result;
        }

        public virtual ServiceResult Edit(T entity)
        {
            ServiceResult result = new ServiceResult();
            try
            {
                (_context as DbContext).Entry(entity).State = System.Data.Entity.EntityState.Modified;
                result = Save();
            }
            catch (Exception e)
            {
                result.Result = ServiceResultStatus.Error;
                result.Messages.Add(e.Message);
            }
            return result;
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = _set.Where(predicate);
            return query;
        }

        public virtual IQueryable<T> GetAll()
        {
            return _set;
        }

        public virtual T GetSingle(int id)
        {

            var result = _set.FirstOrDefault(r => r.Id == id);

            return result; ;
        }

        public virtual ServiceResult Save()
        {
            ServiceResult result = new ServiceResult();
            try
            {
                ( (DbContext)_context).SaveChanges();
            }
            catch (Exception e)
            {
                result.Result = ServiceResultStatus.Error;
                result.Messages.Add(e.Message);
            }

            return result;

        }
    }
}
