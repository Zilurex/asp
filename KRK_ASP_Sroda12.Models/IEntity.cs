﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRK_ASP_Sroda12.Models
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
