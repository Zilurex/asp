﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRK_ASP_Sroda12.Models
{
    public class Sylabus :IEntity<int>
    {

        public int Id { get; set; }

        [Required]
        public string SubjectName { get; set; }

        public string SubjectCode { get; set; }

        public SubjectType SubjectType { get; set; }

        [ForeignKey("FieldOfStudy")]
        public int FieldOfStudy_Id { get; set; }

        public virtual FieldOfStudy FieldOfStudy { get; set; }
    }
}
