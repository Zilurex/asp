﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRK_ASP_Sroda12.Models
{
    public enum SubjectType
    {
        [Display(Name = "Ogólny")]
        Ogolny,
        [Display(Name = "Obowiązkowy")]
        Obowiazkowy,
        [Display(Name = "Obieralny")]
        Obieralny,
    }
}
