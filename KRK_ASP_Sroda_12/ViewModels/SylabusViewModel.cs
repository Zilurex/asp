﻿using KRK_ASP_Sroda12.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KRK_ASP_Sroda_12.ViewModels
{
    public class SylabusViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nazwa przedmiotu")]
        public string SubjectName { get; set; }

        [Required]
        [Display(Name = "Kod przedmiotu")]
        public string SubjectCode { get; set; }

        [Display(Name = "Typ przedmiotu")]
        public SubjectType SubjectType { get; set; }

        [Display(Name = "Kierunek")]
        public int SelectedFieldOfStudyId { get; set; }

        public SelectList FieldOfStudies { get; set; }
    }
}