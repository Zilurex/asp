﻿using KRK_ASP_Sroda12.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KRK_ASP_Sroda_12.ViewModels
{
    public class SylabusIndexViewModel
    {
        public int FieldOfStudyId { get; set; }

        public SelectList FieldOfStudies { get; set; }

        public List<SylabusIndexItemViewModel> Items { get; set; }
    }

    public class SylabusIndexItemViewModel
    {
        public int Id { get; set; }

        public string SubjectName { get; set; }

        public string SubjectCode { get; set; }

        public SubjectType SubjectType { get; set; }
    }
}