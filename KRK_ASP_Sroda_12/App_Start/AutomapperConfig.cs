﻿using KRK_ASP_Sroda_12.ViewModels;
using KRK_ASP_Sroda12.Models;
using KRK_ASP_Sroda12.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KRK_ASP_Sroda_12.App_Start
{
    public class AutomapperConfig
    {
        public static void RegisterMaps()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Sylabus, SylabusViewModel>()
                .ForMember<int>(m => m.SelectedFieldOfStudyId, opt => opt.MapFrom<int>(src => src.FieldOfStudy_Id))
                .ForMember<SelectList>(m => m.FieldOfStudies, opt => opt.ResolveUsing<SelectList>(src =>
                {
                    SelectListResolver<FieldOfStudy> resolver = new SelectListResolver<FieldOfStudy>();
                    return resolver.Resolve(src.FieldOfStudy);
                }
                  ));

                cfg.CreateMap<SylabusViewModel, Sylabus>()
                .ForMember<int>(m => m.FieldOfStudy_Id, opt => opt.MapFrom<int>(src => src.SelectedFieldOfStudyId));

                cfg.CreateMap<Sylabus, SylabusIndexItemViewModel>()
                .ForMember<string>(m => m.SubjectCode, opt => opt.MapFrom<string>(src => src.SubjectCode));
            });

        }
    }
    public class SelectListResolver<TSource> where TSource : IEntity<int>//, IEntityWithName<int>
    {

        public SelectListResolver()
        {

        }

        public SelectList Resolve(TSource source)
        {
            var id = -1;
            if (source != null)
                id = source.Id;
            var _repositoryService = DependencyResolver.Current.GetService<IRepositoryService<TSource>>();

            SelectList result = new SelectList(_repositoryService.GetAll()
                .ToList()
                //.OrderBy(r => r.Name)
                , "Id", "Name", source);

            return result;// _repositoryService.GetSingle(id);
        }
    }
}