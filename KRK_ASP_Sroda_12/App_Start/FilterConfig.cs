﻿using System.Web;
using System.Web.Mvc;

namespace KRK_ASP_Sroda_12
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
