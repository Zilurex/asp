﻿using KRK_ASP_Sroda_12.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KRK_ASP_Sroda_12.Areas.Admin.Controllers
{
    public class UserRolesController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        ApplicationDbContext context = new ApplicationDbContext();

        // GET: Admin/UserRoles
        public ActionResult Index()
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            return View(roleMngr.Roles.ToList());
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }

    internal class RolesStore<T>
    {
        private ApplicationDbContext context;

        public RolesStore(ApplicationDbContext context)
        {
            this.context = context;
        }
    }
}