﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KRK_ASP_Sroda_12.Startup))]
namespace KRK_ASP_Sroda_12
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
