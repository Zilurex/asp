﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using KRK_ASP_Sroda12.Models;
using KRK_ASP_Sroda12.Services;
using System.Linq;

namespace KRK_ASP_Sroda_12.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbContextInitializer());

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public virtual DbSet<Sylabus> Sylabuses { get; set; }
        public virtual DbSet<FieldOfStudy> FieldOfStudies { get; set; }

        public System.Data.Entity.DbSet<KRK_ASP_Sroda_12.ViewModels.SylabusViewModel> SylabusViewModels { get; set; }
    }


    public class ApplicationDbContextInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            context.FieldOfStudies.Add(new FieldOfStudy() { Name = "Informatyka dzienne I Stopień" });
            context.FieldOfStudies.Add(new FieldOfStudy() { Name = "Informatyka zaoczne I Stopień" });
            context.FieldOfStudies.Add(new FieldOfStudy() { Name = "Mechanika dzienne I Stopień" });
            context.FieldOfStudies.Add(new FieldOfStudy() { Name = "Mechanika zaoczne I Stopień" });

            context.SaveChanges();
            var fieldOfStudy = context.FieldOfStudies.First();

            context.Sylabuses.Add(new Sylabus()
            {
                SubjectCode = "IDI01",
                SubjectName = "Test 01",
                SubjectType = SubjectType.Ogolny,
                FieldOfStudy_Id = fieldOfStudy.Id,
            });

            context.Sylabuses.Add(new Sylabus()
            {
                SubjectCode = "IDI02",
                SubjectName = "Test 02",
                SubjectType = SubjectType.Ogolny,
                FieldOfStudy_Id = fieldOfStudy.Id,
            });

            context.SaveChanges();
            base.Seed(context);
        }
    }
}