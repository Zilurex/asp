﻿using KRK_ASP_Sroda_12.Models;
using KRK_ASP_Sroda_12.ViewModels;
using KRK_ASP_Sroda12.Models;
using KRK_ASP_Sroda12.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace KRK_ASP_Sroda_12.Controllers
{
    [Authorize]
    public class SylabusController : Controller
    {
        IRepositoryService<Sylabus> _sylabusService;
        Lazy<IRepositoryService<FieldOfStudy>> _fieldOfStudyService;


        public SylabusController(IRepositoryService<Sylabus> sylabusService,
            Lazy<IRepositoryService<FieldOfStudy>> fieldOfStudyService
            )
        {
            _sylabusService = sylabusService;
            _fieldOfStudyService = fieldOfStudyService;
        }

        public ActionResult Index(int? FieldOfStudyId)
        {
            var sylabuses = _sylabusService.GetAll();

            if(FieldOfStudyId != null)
            {
                sylabuses.Where(x => x.FieldOfStudy_Id == FieldOfStudyId);
            }

            SylabusIndexViewModel viewModel = new SylabusIndexViewModel()
            {
                FieldOfStudies = new SelectList(_fieldOfStudyService.Value.GetAll(), "Id", "Name"),
                Items = sylabuses
                .ToList()
                .Select(r => AutoMapper.Mapper.Map<SylabusIndexItemViewModel>(r))
                .ToList()
            };

            return View(viewModel);
        }

        // GET: Sylabus/Create
        public ActionResult Create()
        {
            SylabusViewModel viewModel = new SylabusViewModel(){ FieldOfStudies = new SelectList(_fieldOfStudyService.Value.GetAll(), "Id", "Name") };
            return View(viewModel);
        }

        // POST: Sylabus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SylabusViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var sylabusToCreate = new Sylabus();

                AutoMapper.Mapper.Map<SylabusViewModel, Sylabus>(viewModel, sylabusToCreate);

                var result = _sylabusService.Add(sylabusToCreate);

                if (result.Result == ServiceResultStatus.Succes)
                {
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var sylabusToEdit = _sylabusService.GetSingle(id.Value);

                if (sylabusToEdit != null)
                {
                    SylabusViewModel viewModel = AutoMapper.Mapper.Map<SylabusViewModel>(sylabusToEdit);
                    viewModel.SelectedFieldOfStudyId = sylabusToEdit.FieldOfStudy_Id;

                    return View(viewModel);
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SylabusViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var sylabusToEdit = _sylabusService.GetSingle(viewModel.Id);
                if (sylabusToEdit != null)
                {
                    AutoMapper.Mapper.Map<SylabusViewModel, Sylabus>(viewModel, sylabusToEdit);
                    sylabusToEdit.FieldOfStudy_Id = viewModel.SelectedFieldOfStudyId;

                    //sylabusToEdit.SubjectName = viewModel.SubjectName;
                    //sylabusToEdit.SubjectCode = viewModel.SubjectCode;
                    //sylabusToEdit.SubjectType = viewModel.SubjectType;

                    var result = _sylabusService.Edit(sylabusToEdit);

                    if (result.Result == ServiceResultStatus.Succes)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            return View(viewModel);
        }

        // GET: Sylabus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Sylabus sylabus = _sylabusService.GetSingle(id.Value);
            SylabusViewModel viewModel = AutoMapper.Mapper.Map<SylabusViewModel>(sylabus);
            if (sylabus == null)
            {
                return HttpNotFound();
            }
            return View(viewModel);
        }


        // GET: Sylabus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var sylabusToDelete = _sylabusService.GetSingle(id.Value);
            if (sylabusToDelete == null)
            {
                return HttpNotFound();
            }

            SylabusViewModel viewModel = AutoMapper.Mapper.Map<SylabusViewModel>(sylabusToDelete);
            return View(viewModel);
        }

        // POST: Sylabus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sylabusToDelete = _sylabusService.GetSingle(id);
            _sylabusService.Delete(sylabusToDelete);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            // if(disposing)
            // context.Dispose();

            base.Dispose(disposing);
        }
    }
}